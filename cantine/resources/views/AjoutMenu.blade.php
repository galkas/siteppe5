<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css') }}">
</head>
<body>
<div class="accueil">
        <center><h1>Ajout menu<h1/></center>
@include("../BarreNavigation")
</br>
</div>
<div class="formulairePersonne">
<ul>
<form action="AjoutMenu" method="post">
{{ csrf_field() }}

<label>Jour
    <select name="jour">
        <option value="Lundi">Lundi</option>
        <option value="Mardi">Mardi</option>
        <option value="Mercredi">Mercredi</option>
        <option value="Jeudi">Jeudi</option>
        <option value="Vendredi">Vendredi</option>
    </select>
</label>
</br>
<input type="date" name="date" required/> date menu    
</br>
<label>Type
    <select name="type">
        <option value="Dejeuner">Dejeuner</option>
        <option value="Diner">Diner</option>
    </select>
</label>
</br>
</br>

<?php
try
{
    $bdd = new PDO('pgsql:host=localhost;dbname=cantine', 'postgres', 'postgres');
}
catch(Exception $e)
{
    die('Erreur : '.$e->getMessage());
}
?>
 
    <label for="entree1">Entrée 1</label><br />
     <select name="entree1" id="entree1">
     <option value="">vide</option>
 
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=1'); //where type="entree"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>
</br>
 
    <label for="entree2">Entrée 2</label><br />
     <select name="entree2" id="entree2">
     <option value="">vide</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=1'); //where type="entree"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>

</br>
 
    <label for="entree3">Entrée 3</label><br />
     <select name="entree3" id="entree3">
     <option value="">vide</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=1'); //where type="entree"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>

</br>
 
    <label for="plat1">Plat 1</label><br />
     <select name="plat1" id="plat1">
     <option value="">vide</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=2'); //where type="plat"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>

</br>
 
    <label for="plat2">Plat 2</label><br />
     <select name="plat2" id="plat2">
     <option value="">vide</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=2'); //where type="plat"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>

</br>
 
    <label for="accompagnement1">Accompagnement 1</label><br />
     <select name="accompagnement1" id="accompagnement1">
     <option value="">vide</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=3'); //where type="accompagnement"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>
</br>
 
    <label for="accompagnement2">Accompagnement 2</label><br />
     <select name="accompagnement2" id="accompagnement2">
     <option value="">vide</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=3'); //where type="accompagnement"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>
</br>
 
    <label for="fromage1">Fromage 1</label><br />
     <select name="fromage1" id="fromage1">
     <option value="">vide</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=4'); //where type="fromage"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>
</br>
 
    <label for="fromage2">Fromage 2</label><br />
     <select name="fromage2" id="fromage2">
     <option value="">vide</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=4'); //where type="fromage"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>
</br>
 
    <label for="dessert1">Dessert 1</label><br />
     <select name="dessert1" id="dessert1">
     <option value="">vide</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=5'); //where type="dessert"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>
</br>
 
    <label for="dessert2">Dessert 2</label><br />
     <select name="dessert2" id="dessert2">
     <option value="">vide</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM plats WHERE type=5'); //where type="dessert"
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>

</br>
 
    <label for="theme">Theme</label><br />
     <select name="theme" id="theme">
     <option value="">aucun</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM themes');
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['intitule']; ?></option>
<?php
}
 
?>
</select>
</br>
 
    <label for="intervenant">Intervenant</label><br />
     <select name="intervenant" id="intervenant">
     <option value="">aucun</option>
<?php
 
$reponse = $bdd->query('SELECT * FROM intervenants');
 
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['id']; ?>"> <?php echo $donnees['nom']; ?></option>
<?php
}
 
?>
</select>

</br>
<br/>
<center><input type="submit" value="Valider le formulaire"/></center>
</form>

</ul>
</div>
