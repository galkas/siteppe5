<!DOCTYPE html>

<html>

<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css')}}">
<head/>
<body>
<div class="accueil">
       <center><h1>Consultation du menu<h1/></center>

@include("BarreNavigation")
<br/>
</div>
<div class="affBien">
<?php

if($_GET['date'] != null){$mydate = $_GET['date'];}
else{$mydate = Date('Y-m-d');}

$mydate2 = new DateTime($mydate);
$mydate2->modify('+1 day');
$mydate2 = $mydate2->format('Y-m-d');

$mydate3 = new DateTime($mydate);
$mydate3->modify('-1 day');
$mydate3 = $mydate3->format('Y-m-d');

echo("<div class='affD'>");

	echo('<form action="AfficheMenu?date='.$mydate2.'" method="post">');
?>
	{{ csrf_field() }}
<?php
	echo('<input type="submit" name="envoi" value="Jour suivant" />');
	echo('</form>');


	echo('<form action="AfficheMenu?date='.$mydate3.'" method="post">');
?>
	{{ csrf_field() }}
<?php
	echo('<input type="submit" name="envoi" value="Jour précédent" />');
	echo('</form>');

echo('</br> </div> </br>');


echo("<div class='affD'>Menu du ".$mydate."</div> </br>");

$idmenu = DB::table('menus')->where('date',$mydate)->select('id')->value('id');

$idtheme = DB::table('menus')->where('date',$mydate)->select('id_theme')->value('id_theme');
$theme = DB::table('themes')->where('id',$idtheme)->select('intitule')->value('intitule');

$idintervenant = DB::table('menus')->where('date',$mydate)->select('id_intervenant')->value('id_intervenant');
$intervenant = DB::table('intervenants')->where('id',$idintervenant)->select('nom')->value('nom');

$plats = App\menu_has_plat::where('menu_id',$idmenu)->select('plat_id')->get();

$jour = DB::table('menus')->where('date',$mydate)->select('jour')->value('jour');

echo("<div class='affD'>");
echo($jour);
echo('</br> </div> </br>');

foreach($plats as $each)
{
	
	$entree = DB::table('plats')->where('id',$each->plat_id)->where('type',1)->select('intitule')->value('intitule');
	if ($entree != null)
	{
		echo("<div class='affD'>");
		echo($entree);
		echo('</br> </div> </br>');
	}
	if ($entree == null){
		$plat = DB::table('plats')->where('id',$each->plat_id)->where('type',2)->select('intitule')->value('intitule');
		if($plat != null) {
			echo("<div class='affD'>");
			echo($plat);
			echo('</br> </div> </br>');
		}
		if($plat == null){
			$accomp = DB::table('plats')->where('id',$each->plat_id)->where('type',3)->select('intitule')->value('intitule');
			if($accomp != null){
				echo("<div class='affD'>");
				echo($accomp);
				echo('</br> </div> </br>');
			}
			if($accomp == null){
				$fro = DB::table('plats')->where('id',$each->plat_id)->where('type',4)->select('intitule')->value('intitule');
				if($fro != null){
					echo("<div class='affD'>");
					echo($fro);
					echo('</br> </div> </br>');
				}
				if($fro == null){
					$des = DB::table('plats')->where('id',$each->plat_id)->where('type',5)->select('intitule')->value('intitule');
					if($des != null){
						echo("<div class='affD'>");
						echo($des);
						echo('</br> </div> </br>');
					}
				}
			}
		}
	}
}
if($theme != null){
echo("<div class='affD'>");
echo($theme);
echo('</br> </div> </br>');
}
if($intervenant != null){
echo("<div class='affD'>");
echo($intervenant);
echo('</br> </div> </br>');
}

?>

</div>
</br>

</body>


</html>
