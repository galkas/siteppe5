<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Accueil');
});

Route::get('/home', function () {
    return view('Accueil');
});

Route::get('/Accueil', function () {
    return view('Accueil');
});

Route::get('/AjoutPlat', function () {
    return view('AjoutPlat');
})->middleware('auth');
Route::post('/AjoutPlat', function () {
	$Annonce = new App\plat;
	$Annonce->intitule = request('intitule');
	$Annonce->description = request('description');
	$Annonce->type = request('type');
	
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutMenu', function () {
    return view('AjoutMenu');
})->middleware('auth');
Route::post('/AjoutMenu', function () {
	$Annonce = new App\menu;
	$Annonce->date = request('date');
	$Annonce->jour = request('jour');
	$Annonce->type = request('type');
	$Annonce->id_theme = request('theme');
	$Annonce->id_intervenant = request('intervenant');
	
	$Annonce->save();

	$toto = DB::table('menus')->max('id');

	$post1 = new App\menu_has_plat;
	$post1->menu_id = $toto;
	$post1->plat_id = request('entree1');

	$post1->save();

	$post2 = new App\menu_has_plat;
	$post2->menu_id = $toto;
	$post2->plat_id = request('entree2');

	$post2->save();

	$post3 = new App\menu_has_plat;
	$post3->menu_id = $toto;
	$post3->plat_id = request('entree3');

	$post3->save();

	$post4 = new App\menu_has_plat;
	$post4->menu_id = $toto;
	$post4->plat_id = request('plat1');

	$post4->save();

	$post5 = new App\menu_has_plat;
	$post5->menu_id = $toto;
	$post5->plat_id = request('plat2');

	$post5->save();

	$post6 = new App\menu_has_plat;
	$post6->menu_id = $toto;
	$post6->plat_id = request('accompagnement1');

	$post6->save();

	$post7 = new App\menu_has_plat;
	$post7->menu_id = $toto;
	$post7->plat_id = request('accompagnement2');

	$post7->save();

	$post8 = new App\menu_has_plat;
	$post8->menu_id = $toto;
	$post8->plat_id = request('fromage1');

	$post8->save();

	$post9 = new App\menu_has_plat;
	$post9->menu_id = $toto;
	$post9->plat_id = request('fromage2');

	$post9->save();


	$post10 = new App\menu_has_plat;
	$post10->menu_id = $toto;
	$post10->plat_id = request('dessert1');

	$post10->save();

	$post11 = new App\menu_has_plat;
	$post11->menu_id = $toto;
	$post11->plat_id = request('dessert2');

	$post11->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutTheme', function () {
    return view('AjoutTheme');
})->middleware('auth');
Route::post('/AjoutTheme', function () {
    $Annonce = new App\theme;
	$Annonce->intitule = request('intitule');
	$Annonce->description = request('description');
	
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutIntervenant', function () {
    return view('AjoutIntervenant');
})->middleware('auth');
Route::post('/AjoutIntervenant', function () {
	$Annonce = new App\intervenant;
	$Annonce->nom = request('nom');
	$Annonce->ville = request('ville');
	
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AfficheMenu', function () {
    return view('afficheMenu');
});
Route::post('/AfficheMenu', function () {
    return view('afficheMenu');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
