<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuHasPlat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_has_plats',function($table) {
		$table->increments('id');
		$table->integer('menu_id');
		$table->foreign('menu_id')
		      ->references('id')
		      ->on('menus');
		$table->integer('plat_id')->nullable();
		$table->foreign('plat_id')
		      ->references('id')
		      ->on('plats');
		$table->timestamps();
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
