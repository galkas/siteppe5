<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Menu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus',function($table) {
		$table->increments('id');
		$table->integer('id_theme')->nullable();
		$table->foreign('id_theme')
		      ->references('id')
		      ->on('themes');
		$table->integer('id_intervenant')->nullable();
		$table->foreign('id_intervenant')
		      ->references('id')
		      ->on('intervenants');
		$table->date('date');
		$table->string('jour',10);
		$table->string('type',10);
		$table->timestamps();
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
